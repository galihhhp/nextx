import { useState } from 'react';
import { connect, useSelector, useDispatch } from 'react-redux';
import {
  putItem,
  removeItem,
  incrementItem,
  decrementItem,
} from 'redux/actions';

const Item = () => {
  const [item, setItem] = useState('');
  const items = useSelector((state) => state.item.items);
  const count = useSelector((state) => state.count.number);
  const dispatch = useDispatch();

  const handleOnChange = (e) => {
    const itemValue = e.target.value;
    setItem(item, itemValue);
    console.log(e.target.value);
  };

  return (
    <div className="container d-flex flex-column justify-content-center align-items-center">
      <div className="w-50 pt-5 mb-5 d-flex flex-column justify-content-center">
        <h4>items: {items}</h4>
        <input className="my-2" type="text" onChange={handleOnChange} />
        <button className="my-4" onClick={(item) => dispatch(putItem(item))}>
          add item
        </button>
        <button onClick={() => dispatch(removeItem())}>remove item</button>
      </div>
      <div>
        <h4 className="text-center">number: {count}</h4>
        <button
          className="btn btn-danger"
          onClick={() => dispatch(decrementItem())}
        >
          Decrement
        </button>
        <button
          className="btn btn-success"
          onClick={() => dispatch(incrementItem())}
        >
          Increment
        </button>
      </div>
    </div>
  );
};

// const mapStateToProps = (state) => {
//   return {
//     items: state.items,
//     number: state.number,
//   };
// };

// const mapDispatchToProps = (dispatch, items) => {
//   return {
//     putItem: () => dispatch(putItem(items)),
//     removeItem: () => dispatch(removeItem()),
//   };
// };

// export default connect(mapStateToProps, mapDispatchToProps)(Item);

export default Item;
