import { combineReducers } from 'redux';
import itemReducer from 'redux/itemsReducers';
import countReducers from 'redux/countReducers';

const rootReducer = combineReducers({
  item: itemReducer,
  count: countReducers,
});

export default rootReducer;
