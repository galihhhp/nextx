import { INCREMENT, DECREMENT, PUT_ITEM, REMOVE_ITEM } from 'redux/types';

export const putItem = (item) => ({
  type: PUT_ITEM,
  item,
});

export const removeItem = () => ({
  type: REMOVE_ITEM,
});

export const incrementItem = () => ({
  type: INCREMENT,
});

export const decrementItem = () => ({
  type: DECREMENT,
});
