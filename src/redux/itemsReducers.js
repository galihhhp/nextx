import { PUT_ITEM, REMOVE_ITEM } from 'redux/types';

const initialState = {
  items: ['hallo', 'hold'],
};

const putItem = (state, action) =>
  Object.assign({}, state, {
    items: state.items.push(action.items),
  });

const removeItem = (state) =>
  Object.assign({}, state, {
    items: state.items.pop(),
  });

const itemReducer = (state = initialState, action) => {
  switch (action.type) {
    case PUT_ITEM:
      return putItem(state, action);
    case REMOVE_ITEM:
      return removeItem(state);
    default:
      return state;
  }
};

export default itemReducer;
