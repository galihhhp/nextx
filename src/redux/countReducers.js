import { INCREMENT, DECREMENT } from 'redux/types';

const initialState = {
  number: 1,
};

const increment = (state) =>
  Object.assign({}, state, {
    number: (state.number += 1),
  });

const decrement = (state) =>
  Object.assign({}, state, {
    number: (state.number = 0 && state.number - 1),
  });

const countReducers = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT:
      return increment(state);
    case DECREMENT:
      return decrement(state);
    default:
      return state;
  }
};

export default countReducers;
